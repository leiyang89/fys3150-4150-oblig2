#include <iostream>
#include <armadillo>
//#include <lib.h>
#include <lib.cpp>
#include "time.h"

using namespace std;
using namespace arma;




int main()
{
	double *e, *d, **z;
	double rho_max = 5; //rho_max, vary towards inf
	int n = 160;	//num step
	double h = rho_max/((double) n); //step length
	double non_diag = -1.0/(h*h); // size of non-diagonal, tridiagonal elements
	double diag1 = 2.0/(h*h); // first part of diagonal elements
	d = new double[n-1];
	e = new double[n-1];
	z = (double **) matrix(n-1, n-1, sizeof(double));
	for (int i = 0; i < (n-1); i++) {
		d[i] = diag1 +(i+1.0)*(i+1.0)*h*h;
		e[i] = non_diag;
		z[i][i] = 1.0;
		for (int j = i+1; j < (n-1); j++) {
			z[i][j] = 0.0;
		}
	}
	//start timer
	clock_t start, finish;  //  declare start and final time
    start = clock();
	tqli(d,e,n-2,z);
	//end timer
    finish = clock();
    double time = ( (1.*finish - 1.*start)/CLOCKS_PER_SEC );
	vec D = zeros<vec>(n-1);
	for (int i = 0; i < (n-1); i++) {
		D(i) = d[i];
	}
	cout << sort(D) << endl;
	cout << time << endl;
	return 0;
}
















