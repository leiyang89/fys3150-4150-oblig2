'''
Python output file: remember to create rho spacings based on rho max and n
Remember to read eigenvalue then n-1 eigenvector points, then new array
'''
from scitools.std import *
import sys

#read and write to function all eigenfunctions in all .dat files
#noninteracting smallest omega to highest, then interacting smallest to highest

#read data

#input data file you want to plot as numerical solution
f = open('noninteracting_electrons__1.dat','r')
n1 = int(f.readline())
y11 = zeros(n1-1);
y12 = zeros(n1-1);
y13 = zeros(n1-1);
rho_max1 = float(f.readline())
omega1 =  float(f.readline())
x1 = linspace(rho_max1/(1.0*n1),(n1-1)*rho_max1/(1.0*n1), n1-1)

eigval11 =  float(f.readline())
for i in range(n1-1):
	y11[i] = float(f.readline())

eigval12 =  float(f.readline())
for i in range(n1-1):
	y12[i] = float(f.readline())

eigval13 =  float(f.readline())
for i in range(n1-1):
	y13[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('noninteracting_electrons_5.dat','r')
n2 = int(f.readline())
y21 = zeros(n2-1);
y22 = zeros(n2-1);
y23 = zeros(n2-1);
rho_max2 = float(f.readline())
omega2 =  float(f.readline())
x2 = linspace(rho_max2/(1.0*n2),(n2-1)*rho_max2/(1.0*n2), n2-1)

eigval21 =  float(f.readline())
for i in range(n2-1):
	y21[i] = float(f.readline())

eigval12 =  float(f.readline())
for i in range(n2-1):
	y22[i] = float(f.readline())

eigval23 =  float(f.readline())
for i in range(n2-1):
	y23[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('noninteracting_electrons1.dat','r')
n3 = int(f.readline())
y31 = zeros(n3-1);
y32 = zeros(n3-1);
y33 = zeros(n3-1);
rho_max3 = float(f.readline())
omega3 =  float(f.readline())
x3 = linspace(rho_max3/(1.0*n3),(n3-1)*rho_max3/(1.0*n3), n3-1)

eigval31 =  float(f.readline())
for i in range(n3-1):
	y31[i] = float(f.readline())

eigval32 =  float(f.readline())
for i in range(n3-1):
	y32[i] = float(f.readline())

eigval33 =  float(f.readline())
for i in range(n3-1):
	y33[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('noninteracting_electrons5.dat','r')
n4 = int(f.readline())
y41 = zeros(n4-1);
y42 = zeros(n4-1);
y43 = zeros(n4-1);
rho_max4 = float(f.readline())
omega4 =  float(f.readline())
x4 = linspace(rho_max4/(1.0*n4),(n4-1)*rho_max4/(1.0*n4), n4-1)

eigval41 =  float(f.readline())
for i in range(n4-1):
	y41[i] = float(f.readline())

eigval42 =  float(f.readline())
for i in range(n4-1):
	y42[i] = float(f.readline())

eigval43 =  float(f.readline())
for i in range(n4-1):
	y43[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('interacting_electrons__1.dat','r')
n5 = int(f.readline())
y51 = zeros(n5-1);
y52 = zeros(n5-1);
y53 = zeros(n5-1);
rho_max5 = float(f.readline())
omega5 =  float(f.readline())
x5 = linspace(rho_max5/(1.0*n5),(n5-1)*rho_max5/(1.0*n5), n5-1)

eigval51 =  float(f.readline())
for i in range(n5-1):
	y51[i] = float(f.readline())

eigval52 =  float(f.readline())
for i in range(n5-1):
	y52[i] = float(f.readline())

eigval53 =  float(f.readline())
for i in range(n5-1):
	y53[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('interacting_electrons_5.dat','r')
n6 = int(f.readline())
y61 = zeros(n6-1);
y62 = zeros(n6-1);
y63 = zeros(n6-1);
rho_max6 = float(f.readline())
omega6 =  float(f.readline())
x6 = linspace(rho_max6/(1.0*n6),(n6-1)*rho_max6/(1.0*n6), n6-1)

eigval61 =  float(f.readline())
for i in range(n6-1):
	y61[i] = float(f.readline())

eigval62 =  float(f.readline())
for i in range(n6-1):
	y62[i] = float(f.readline())

eigval63 =  float(f.readline())
for i in range(n6-1):
	y63[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('interacting_electrons1.dat','r')
n7 = int(f.readline())
y71 = zeros(n7-1);
y72 = zeros(n7-1);
y73 = zeros(n7-1);
rho_max7 = float(f.readline())
omega7 =  float(f.readline())
x7 = linspace(rho_max7/(1.0*n7),(n7-1)*rho_max7/(1.0*n7), n7-1)

eigval71 =  float(f.readline())
for i in range(n7-1):
	y71[i] = float(f.readline())

eigval72 =  float(f.readline())
for i in range(n7-1):
	y72[i] = float(f.readline())

eigval73 =  float(f.readline())
for i in range(n7-1):
	y73[i] = float(f.readline())
f.close()

#read data

#input data file you want to plot as numerical solution
f = open('interacting_electrons5.dat','r')
n8 = int(f.readline())
y81 = zeros(n8-1);
y82 = zeros(n8-1);
y83 = zeros(n8-1);
rho_max8 = float(f.readline())
omega8 =  float(f.readline())
x8 = linspace(rho_max8/(1.0*n8),(n8-1)*rho_max8/(1.0*n8), n8-1)

eigval81 =  float(f.readline())
for i in range(n8-1):
	y81[i] = float(f.readline())

eigval82 =  float(f.readline())
for i in range(n8-1):
	y82[i] = float(f.readline())

eigval83 =  float(f.readline())
for i in range(n8-1):
	y83[i] = float(f.readline())
f.close()

#PLOT all noninteracting on the same plot
#plot all of same eigenvalue on the same plot

#noninteracting ground state
plot(x1,y11)
hold('on')
plot(x2,y21)
plot(x3,y31)
plot(x4,y41)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega1,eigval11,omega2,eigval21,omega3,eigval31,omega4,eigval41))
title('Non-interacting ground state')
savefig('nonintground.png')
hold('off')

#noninteracting 1. excited
plot(x1,y12)
hold('on')
plot(x2,y22)
plot(x3,y32)
plot(x4,y42)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega1,eigval12,omega2,eigval22,omega3,eigval32,omega4,eigval42))
title('Non-interacting 1. excited state')
savefig('nonintfirst.png')
hold('off')


#noninteracting 2. excited
plot(x1,y13)
hold('on')
plot(x2,y23)
plot(x3,y33)
plot(x4,y43)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega1,eigval13,omega2,eigval23,omega3,eigval33,omega4,eigval43))
title('Non-interacting 2. excited state')
savefig('nonintsecond.png')
hold('off')

#interacting ground
plot(x5,y51)
hold('on')
plot(x6,y61)
plot(x7,y71)
plot(x8,y81)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega5,eigval51,omega6,eigval61,omega7,eigval71,omega8,eigval81))
title('Interacting ground state')
savefig('intground.png')
hold('off')

#interacting 1. excited
plot(x5,y52)
hold('on')
plot(x6,y62)
plot(x7,y72)
plot(x8,y82)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega5,eigval52,omega6,eigval62,omega7,eigval72,omega8,eigval82))
title('Interacting 1. excited state')
savefig('intfirst.png')
hold('off')

#interacting 2. excited
plot(x5,y53)
hold('on')
plot(x6,y63)
plot(x7,y73)
plot(x8,y83)
xlabel('rho(r)')
ylabel('psi(rho)')
legend('omega = %d, eigenvalue = %d', 'omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d','omega = %d, eigenvalue = %d' %(omega5,eigval53,omega6,eigval63,omega7,eigval73,omega8,eigval83))
title('Interacting 2. excited state')
savefig('intsecond.png')
hold('off')


