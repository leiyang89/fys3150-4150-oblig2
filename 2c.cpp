#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;


double maxoffdiag(mat A, int n, int *k, int *l) {
	double max = 0.0;
	for (int i = 0; i < n-2; i++) { //last row is 'blocked' to the right
		for (int j = i+1; j< n-1; j++) {
			if (fabs(A(i,j)) > max) {
				max = fabs(A(i,j));
				*l = i;
				*k = j;
				
			}
		}
	}
	return max;
}

void findCS(mat A, int k, int l, int n, double * c, double * s) {
	if (A(k,l) != 0.0) {
		double t, tau;
		tau = (A(l,l) - A(k,k))/(2.0*A(k,l));
		//choose smallest root
		if (tau > 0) {
			t = -tau + sqrt(1.0 + tau*tau);
		}
		else {
			t = -tau - sqrt(1.0 + tau*tau);	
		}
		*c = 1.0/sqrt(1+t*t);
		*s = (1.0/sqrt(1+t*t))*t;
	}
	else {
		*c = 1.0;
		*s = 0.0;
	}
}


int main()
{
	double omega = 5.0;
	//set up matrix A, the 'operator' matrix in (2)
	double rho_max = 3.0; //rho_max, vary towards inf
	int n = 200;	//num step
	double h = rho_max/((double) n); //step length
	double non_diag = -1.0/(h*h); // size of non-diagonal, tridiagonal elements
	mat A = zeros<mat>(n-1,n-1);
	double diag1 = 2.0/(h*h); // first part of diagonal elements
	//assign values to A elements	
	for (int i = 0; i < (n-1); i++) {
		//assign diag
		A(i,i) = diag1 + omega*omega*(i+1.0)*(i+1.0)*h*h + 1.0/((i+1.0)*h);
		//lower trid		
		if (i > 0) {
			A(i,i-1) = non_diag;
		} 
		//upper trid
		if (i < (n-2)) {
			A(i,i+1) = non_diag;
		}
	}

	//matrix R for eigenvectors
	//mat R = zeros<mat>(n-1,n-1);
	//for (int i = 0; i < (n-1); i++) {
	//	R(i,i) = 1.0;
	//}


	//initialize jacobi
	int k, l;
	double tolerance = pow(10,-14);
	double max_iterations = (double) (n-1) * (double) (n-1) * (double) (n-1);
	int iterations = 0;
	double max_offdiag;

	max_offdiag = maxoffdiag(A, n, &k, &l);
	double a_kk, a_ll, a_ik, a_il, r_ik, r_il;
		
	//loop rotations, check against maxoffdiag
	while ((fabs(pow(max_offdiag,2)) > tolerance) && ((double) iterations < max_iterations)) {
		max_offdiag = maxoffdiag(A,n, &k, &l);
		double c,s;
		findCS(A,k,l,n, &c, &s);
		//rotate below
		a_kk = A(k,k);
		a_ll = A(l,l);
		//change kl elements		
		A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
		A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
		A(k,l) = 0.0;
		A(l,k) = 0.0;
		//the rest of the elements
		for (int i = 0; i < n-1; i++) {
			if ((i != k) && (i != l)) {			
			a_ik = A(i,k);
			a_il = A(i,l);
			A(i,k) = c*a_ik - s*a_il;
			A(k,i) = A(i,k);
			A(i,l) = c*a_il + s*a_ik;
			A(l,i) = A(i,l);
			}
			//insert eigenvectors
			//r_ik = R(i,k);
			//r_il = R(i,l);
			//R(i,k) = c*r_ik - s*r_il;
			//R(i,l) = c*r_il + s*r_ik;
		}		
		iterations++;
	}
	cout << sort(A.diag()) << endl;
	return 0;
}
















