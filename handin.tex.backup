\documentclass[a4wide,12pt]{article}

\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}

\begin{document}

\title{Project 2: Schr\"odinger's equation for 2 electrons in a 3D harmonic oscillator well
: source files to be found on bitbucket under leiyang89}
\author{Lei Yang}
\date{6.10.2013}
\maketitle
\section*{Abstract}

\section*{Introduction}
The project aims to solve one of the easiest realistic problems described by the
S. eq., using simple, well-known numerical algorithms.
Jacobi's method employing Givens rotation, together with derivation from first
principles, are employed in finding the approximate 
eigenvalues and eigenkets of a single, and later dual, electron(s) stuck 
in a three-dimensional harmonic potential well. This is a simplified simulation
of real electrons confined in small spaces - quantum dots. To make life easy, 
the project covers only the spherically symmetric case $l = 0$.  

\section*{The Setup}
(NOTE: Almost all of this is taken from project description, remember to link
in bibliography.)
\newline
As described in detail in the project description, the
radial part of the S.eq. for a single electron
under harmonic oscillator potential influence can be rewritten as
\begin{equation}
   -\frac{d^2}{d\rho^2} u(\rho) + \rho^2u(\rho)  = \lambda u(\rho) ,
\end{equation} 
with dimensionless variable
\begin{equation}
  \rho = (1/\alpha) r, 
\end{equation} 
and modified eigenvalues 
\begin{equation}
  \lambda = \frac{2m\alpha^2}{\hbar^2}E,
\end{equation} 
which for the known lower energy eigenvalues 
\begin{equation}
 E_{n}=  \hbar \sqrt{\frac{k}{m}} \left(2n+\frac{3}{2}\right),
\end{equation} 
and 
\begin{equation}
 \alpha = \left(\frac{\hbar^2}{mk}\right)^{1/4},
\end{equation} 
yield 
\begin{equation}
  \lambda = 4n + 3. 
\end{equation} 
\newline
The discretized version of the second derivative is simulated using
first principles and an imperfect limit for h: 
\begin{equation}
      u''=\frac{u(\rho+h) -2u(\rho) +u(\rho-h)}{h^2} +O(h^2),
    \label{eq:diffoperation} 
\end{equation} 
The constant h is the differential step length, and is defined as 
\begin{equation}
  h=\frac{\rho_{\mathrm{max}}-\rho_{\mathrm{min}} }{n_{\mathrm{step}}}.
\end{equation}
For h to simulate a differential quantity correctly, it needs to be much smaller than $\rho$. 
On the other hand, for $\rho$ to meaningfully describe the radial distance as it moves towards
infinity, $ \rho_{\mathrm{max}} $ needs to be big. There is thus a competition between the values
that may require the coder to experiment to find a stable solution. 
\newline
This simplest implementation of a second derivative only depends on the previous and next values of
the function to find the discretized second derivative at any point. It is therefore 
expected that the operator takes the form of a tridiagonal matrix like in project 1. 
The final form is expressed as
\begin{equation}
d_iu_i+e_{i-1}u_{i-1}+e_{i+1}u_{i+1}  = \lambda u_i,
\end{equation}
with the diagonal terms
\begin{equation}
 d_i=\frac{2}{h^2}+{\rho_i}^2,
\end{equation}
and non-diagonal terms
\begin{equation}
 e_i=-\frac{1}{h^2}.
\end{equation}

The nice thing is that the operator is quite general, and it turns out that by tweaking 
the potential term only, the two-electron system may be investigated using the same code. 
\newline
For two electrons in a harmonic oscillator well, the S.eq. may be written as (using two-electron
energy eigenvalues and eigenkets)
\begin{equation}
\left(  -\frac{\hbar^2}{2 m} \frac{d^2}{dr_1^2} -\frac{\hbar^2}{2 m} \frac{d^2}{dr_2^2}+ \frac{1}{2}k r_1^2+ \frac{1}{2}k r_2^2\right)u(r_1,r_2)  = E^{(2)} u(r_1,r_2) .
\end{equation}
This may be simplified to 
\begin{equation}
 -\frac{d^2}{d\rho^2} \psi(\rho) + \omega_r^2\rho^2\psi(\rho) +\frac{1}{\rho}\psi(\rho) = \lambda \psi(\rho),
\end{equation}
where
\begin{equation}
\omega_r^2=\frac{1}{4}\frac{mk}{\hbar^2} \alpha^4,
\end{equation}
is a parameter which reflects oscillator potential strength.
\begin{equation}
\alpha = \frac{\hbar^2}{m\beta e^2},
\end{equation}
and
\begin{equation}
\lambda = \frac{m\alpha^2}{\hbar^2}E
\end{equation}
now have redefined values. 
\newline
Note that the removal of the $\frac{1}{\rho}$ term returns the function to the form of a
non-interacting system. 
The idea is then to make a program to simulate the single-electron system, 
analyse its efficiency and compare its results with established eigenvalue code in Armadillo,
and then to apply the program to the two-electron system, with the modified potential
\begin{equation}
\lambda = \frac{m\alpha^2}{\hbar^2}E
\end{equation}

\section*{The Code}
The Jacobi algorithm is discussed in Chapter 7 of the lecture notes. It relies on a series of similarity
transformations on the symmetric operator to reduce it to a diagonal matrix, where the 
eigenvalues of the operator will be along the diagonal. 
\newline
Givens rotations are plane rotations around an angle. The angle is chosen on each rotation 
so it eliminates one pair, $b_{kl}$ of matrix elements from the transformed operator per iteration.
The norm of the off-diagonal elements, often simplified 
to the largest possible if all elements had the value of the largest off-diagonal,
 are then compared to a set tolerance. The rotations continue until the offdiagonal is below
the tolerance, and the program outputs the diagonal matrix with eigenvalues. 
\newline
The operator was defined in (2). An armadillo assignation could look like:
\begin{lstlisting}[title={Define operator A}]
#include <iostream>
#include <armadillo>


using namespace std;
using namespace arma;
...
	double rho_max = 20000; //rho_max, vary towards inf
	int n = 400;	//num step
	double h = rho_max/((double) n); //step length
	double non_diag = -1.0/(h*h); // size of off-diag elements
	mat A = zeros<mat>(n-1,n-1);
	double diag1 = 2.0/(h*h); // first part of diagonal elements
	//assign values to A elements	
	for (int i = 0; i < (n-1); i++) {
		//assign diag
		A(i,i) = diag1 + (i+1.0)*(i+1.0)*h*h;
		//lower trid		
		if (i > 0) {
			A(i,i-1) = non_diag;
		} 
		//upper trid
		if (i < (n-2)) {
			A(i,i+1) = non_diag;
		}
	}
...
\end{lstlisting}   
The values assigned for n and $\rho_{max}$ would have to be manually adjusted until 
the Jacobian code itself returned stable values. 
\newline
Calculation of the off-diagonal norm for every rotation would be time-consuming, so
the alternative calculation utilizing the largest off-diagonal value is used. 
The largest off-diagonal element needs to be found anyway for k,l selection.

\begin{lstlisting}[title={find maxoffdiag}]
double maxoffdiag(mat A, int n, int *k, int *l) {
	double max = 0.0;
	for (int i = 0; i < n-2; i++) { //last row is 'blocked' to the right
		for (int j = i+1; j< n-1; j++) {
			if (fabs(A(i,j)) > max) {
				max = fabs(A(i,j));
				*l = i;
				*k = j;
				
			}
		}
	}
	return max;
} 
\end{lstlisting}  
Comparison is then made against the tolerance as a check condition for the rotation loop:
\begin{lstlisting}[title={iterate rotations}]
while ((fabs(pow(max_offdiag,2)) > tolerance) && 
((double) iterations < max_iterations)) {
		max_offdiag = maxoffdiag(A,n, &k, &l);
		double c,s;
		findCS(A,k,l,n, &c, &s);
		//rotate below
		a_kk = A(k,k);
		a_ll = A(l,l);
		//change kl elements		
		A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
		A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
		A(k,l) = 0.0;
		A(l,k) = 0.0;
		//the rest of the elements
		for (int i = 0; i < n-1; i++) {
			if ((i != k) && (i != l)) {			
			a_ik = A(i,k);
			a_il = A(i,l);
			A(i,k) = c*a_ik - s*a_il;
			A(k,i) = A(i,k);
			A(i,l) = c*a_il + s*a_ik;
			A(l,i) = A(i,l);
			}
		iterations++;
...
...
\end{lstlisting}  
The rotational constants s=sin and c=cos are themselves assigned as
\begin{lstlisting}[title={sin, cos assignments}]
void findCS(mat A, int k, int l, int n, double * c, double * s) {
	if (A(k,l) != 0.0) {
		double t, tau;
		tau = (A(l,l) - A(k,k))/(2.0*A(k,l));
		//choose smallest root
		if (tau > 0) {
			t = -tau + sqrt(1.0 + tau*tau);
		}
		else {
			t = -tau - sqrt(1.0 + tau*tau);	
		}
		*c = 1.0/sqrt(1+t*t);
		*s = (1.0/sqrt(1+t*t))*t;
	}
	else {
		*c = 1.0;
		*s = 0.0;
	}
}
 \end{lstlisting}  
The root t is always minimized in order to ensure a stable algorithm. This can be seen from the expression
given for the difference between the matrices B and A, with B as the transformed operator:
\begin{equation}
 ||{\bf B}-{\bf A}||_F^2=4(1-c)\sum_{i=1,i\ne k,l}^n(a_{ik}^2+a_{il}^2) +\frac{2a_{kl}^2}{c^2}.
\end{equation}
Since 
\begin{equation}
 t = -\tau \pm \sqrt{1+\tau^2},
\end{equation}
choosing the smaller root insures that t is between 0 and 1, because the term under
the square root will be less or equal to 1 larger than tau. Using
\begin{equation}
 c = \frac{1}{\sqrt{1+t^2}},
\end{equation}
it is seen that c is between 1 and $\frac{1}{\sqrt{2}}$. Since this is in radians, the absolute angle is then
below or right at $\frac{\pi}{4}$. 
Referring to (18) for the difference between the operator and the transformed operator, the 
$1-c$ factor will be minimized for the largest c, since cos is known to be smaller or equal to
1. The smaller t is the one that maximizes c, so this is the reason to minimize t. 
\newline
The tqli Householder's method implementation will be compared with the Jacobi method. 
To call tqli, vectors and matrices should be recast to arrays (using pointers):
\begin{lstlisting}[title={calling tqli}]
double *e, *d, **z;
	double rho_max = 5; //rho_max, vary towards inf
	int n = 160;	//num step
	double h = rho_max/((double) n); //step length
	double non_diag = -1.0/(h*h); // size of non-diagonal, tridiagonal elements
	double diag1 = 2.0/(h*h); // first part of diagonal elements
	d = new double[n-1];
	e = new double[n-1];
	z = (double **) matrix(n-1, n-1, sizeof(double));
	for (int i = 0; i < (n-1); i++) {
		d[i] = diag1 +(i+1.0)*(i+1.0)*h*h;
		e[i] = non_diag;
		z[i][i] = 1.0;
		for (int j = i+1; j < (n-1); j++) {
			z[i][j] = 0.0;
		}
	}
	tqli(d,e,n-2,z);
 \end{lstlisting}  

\section*{Results and Discussion}
Before running the algorithm, it should be checked against a safe eigenvalue solver to insure
that it works as specified. The armadillo eigenvalue solver for symmetric matrices may easily
be implemented:  
\begin{lstlisting}[title={Armadillo eigenvalue solver}]
	vec eigval;
	mat eigvec;
	eig_sym(eigval, eigvec, A);
 \end{lstlisting}
A test for n = 100 and $\rho_{max} = 10000$ for instance gave:
\begin{lstlisting}[title={Armadillo method}]
[leiy@geos oblig2]$ ./2a
   1.0000e+04
   4.0000e+04
   9.0000e+04
   1.6000e+05
   2.5000e+05
   3.6000e+05
   4.9000e+05
   6.4000e+05
 \end{lstlisting}
and 
\begin{lstlisting}[title={Jacobian method}]

 1.0000e+04
   4.0000e+04
   9.0000e+04
   1.6000e+05
   2.5000e+05
   3.6000e+05
   4.9000e+05
   6.4000e+05

 \end{lstlisting}

So the two methods seem to give the same output. 
\newline
The value for $\rho_{max}$ is in this case way too high. After playing around, I found n in 
the hundreds range and $\rho_{max}$ in the tens to work fairly well. Generally, smaller rho 
numbers gave more correct leading digits, and I settled on 5. $n=160$ was then the smallest
matrix size that gave correct eigenvalues with four leading digits for the lowest three
eigenvalues. 
\begin{lstlisting}[title={Sorted Eigenvalues for n = 160, rhomax = 5}]

[leiy@geos oblig2]$ ./2a
   2.9997e+00
   6.9985e+00
   1.0996e+01
   1.4999e+01
   1.9062e+01
   2.3407e+01
   2.8361e+01
   3.4103e+01
   4.0665e+01
   4.8036e+01
   5.6205e+01
   6.5160e+01
   7.4894e+01


 \end{lstlisting}   
The number of similarity transformations needed to reach a diagonal seems to go as $13n^2 - 7n$: 
\begin{lstlisting}[title={Sorted Eigenvalues for n = 160, rhomax = 5}]
x         y
10.0    100
20.0    531
30.0   1252
40.0   2335
50.0   3713
60.0   5433
70.0   7530
80.0   9787
90.0  12484
100.0 15602
110.0 18882
120.0 22539
130.0 26493
140.0 30852
150.0 35415
160.0 40243
 \end{lstlisting} 

\begin{center}
 \includegraphics{./dependency.png}
 % dependency.png: 812x612 pixel, 100dpi, 20.62x15.54 cm, bb=0 0 585 441
\end{center}
Jacobi's method solutions are expected to be quite a bit slower than Householder's algorithm. 
A comparison with tqli in lib.cpp for the same $n = 160, \rho_{max} = 5$:
\begin{lstlisting}[title={tqli-sorted Eigenvalues for n = 160, rhomax = 5}]
[leiy@geos oblig2]$ ./2b
   2.9997e+00
   6.9985e+00
   1.0997e+01
   1.5000e+01
   1.9073e+01
   2.3455e+01
   2.8474e+01
   3.4295e+01
   4.0945e+01
 \end{lstlisting}   
The accuracy is slightly better for the first three eigenvalues. Comparing the times, we 
find tqli uses 0.03 s while Jakobi uses 1 s. It is therefore clear that as matrix sizes increase past the tens, 
the Jakobi method will be very inefficient. However, had the operator matrix not been 
tridiagonal, we couldn't have solved it using tqli. 
\newline
For the non-interacting double-electron system, the potential is modified accordingly. 
The lowest $\omega_r$ at 0.01 has the most unstable eigenvalues. If this one can be satisfied,
the $\omega_r$ at 1 and 5  should also have eigenvalues computable for the given parameters.
\newline
For $\omega_r = 0.01$, $\rho_{max} = 3$, n = 200 gave stable results. The first 3 eigenvalues
for the respective $\omega_r$ are then:  
\begin{lstlisting}[title={$\omega_r = 0.01$}] 
   1.0969e+00
   4.3864e+00
   9.8681e+00
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 0.5$}] 
   1.6845e+00
   5.1192e+00
   1.0614e+01
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 1$}] 
   3.0121e+00
   7.3280e+00
   1.2945e+01
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 5$}] 
  1.4998e+01
   3.4991e+01
   5.4979e+01
 \end{lstlisting}

Keeping the parameters, we now look at the lowest state of the interacting pair of electrons. 
The values found were:
\begin{lstlisting}[title={$\omega_r = 0.01$}] 
    1.8628e+00
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 0.5$}] 
    2.5323e+00
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 1$}] 
     4.0801e+00
 \end{lstlisting}
\begin{lstlisting}[title={$\omega_r = 5$}] 
       1.7447e+01
 \end{lstlisting}
\section*{Conclusion}

\section*{Bibliography}
Fys3150 Lecture Notes 2012 and Project 2 description, Morten Hjorth-Jensen. 
\end{document}


