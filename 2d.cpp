#include <iostream>
#include <armadillo>
//#include <lib.h>
#include <lib.cpp>
#include <iostream>
#include <fstream>

using namespace std;
using namespace arma;




int main()
{
	double omega = 5.0;
	double *e, *d, **z;
	double rho_max = 3; //rho_max, vary towards inf
	int n = 200;	//num step
	double h = rho_max/((double) n); //step length
	double non_diag = -1.0/(h*h); // size of non-diagonal, tridiagonal elements
	double diag1 = 2.0/(h*h); // first part of diagonal elements
	d = new double[n-1];
	e = new double[n-1];
	z = (double **) matrix(n-1, n-1, sizeof(double));
	for (int i = 0; i < (n-1); i++) {
		//interacting case
		//d[i] = diag1 + omega*omega*(i+1.0)*(i+1.0)*h*h + 1.0/((i+1.0)*h);

		//non-interacting case
		d[i] = diag1 + omega*omega*(i+1.0)*(i+1.0)*h*h;

		e[i] = non_diag;
		z[i][i] = 1.0;
		for (int j = i+1; j < (n-1); j++) {
			z[i][j] = 0.0;
		}
	}

	tqli(d,e,n-2,z);

	vec D = zeros<vec>(n-1);
	//convert arrays to matrices
	for (int i = 0; i < (n-1); i++) {
		D(i) = d[i];
	}
	for (int i =0; i< (n-1); i++) {
		for (int j = 0; j < (n-1); j++) {
			
		}
	}
	vec sorted = sort(D);
	int ground, exci_1, exci_2;
	//find indices in D corresponding to the first three sorted values
	for (int i = 0; i< (n-1); i++) {
		if (D(i) == sorted(0)) {
			ground = i;
		}
		if (D(i) == sorted(1)) {
			exci_1 = i;
		}
		if (D(i) == sorted(2)) {
			exci_2 = i;
		}
	}
	//use indices to access appropriate eigenvectors
	vec ground_state(n-1);
	vec exci1_state(n-1);
	vec exci2_state(n-1);
	for (int i =0; i< (n-1); i++) {
		ground_state(i) = z[i][ground];
		exci1_state(i) = z[i][exci_1];	
		exci2_state(i) = z[i][exci_2];
	}
	//write out to table
	//Write to file to plot
	//output n, rho_max, omega, loop (eig_val, then eigvec points), then repeat
	ofstream outfile;
	outfile.open("noninteracting_electrons5.dat");
	outfile << n << endl;
	outfile << rho_max << endl;
	outfile << omega << endl;	
	//ground state
	outfile << sorted(0) << endl;
	for (int i = 0; i < (n-1); i++) {
		outfile << ground_state(i) << endl; 
	}
	//first excited state
	outfile << sorted(1)<< endl;
	for (int i = 0; i < (n-1); i++) {
		outfile << exci1_state(i) << endl; 
	}
	//second excited state
	outfile << sorted(2) << endl;
	for (int i = 0; i < (n-1); i++) {
		outfile << exci2_state(i) << endl; 
	}
	outfile.close();
	
	return 0;
}


